"use strict";

/*let name='Regina';
let url=`images/${name.toLowerCase()}.jpg`;
console.log(url);
let html=

`<article class="pizzaThumbnail">
<a href="${url}">
<img src="${url}"/>
<section>${name}</section>
</a>`;
console.log(html);
document.querySelector('.pageContent').innerHTML = html;
*/
//const data = ['Regina', 'Napolitaine', 'Spicy'];
//boucle for

/*let html="";
for(let i=0; i<data.length; i++){
    let name=data[i];
    let url=`images/${name.toLowerCase()}.jpg`;
    console.log(url);
    html=html+`<article class="pizzaThumbnail">
            <a href="${url}">
                  <img src="${url}"/>
                  <section>${name}</section>
            </a>
        </article>`;
    console.log(html);
    
}
*/
//boucle Array.map

/*let html=data.map(function(pizza) {
    let name= pizza;
    let url=`images/${name.toLowerCase()}.jpg`;
    return `<article class="pizzaThumbnail">
        <a href="${url}">
        <img src="${url}"/>
      <section>${name}</section>
</a>
</article>`}).join("");

console.log(html);


document.querySelector('.pageContent').innerHTML = html;
*/
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
var html2 = '';
var euro = new Intl.NumberFormat('fr-FR', {
  style: 'currency',
  currency: 'EUR',
  minimumFractionDigits: 2
});
data.sort(function (a, b) {
  var textA = a.name.toUpperCase();
  var textB = b.name.toUpperCase();
  return textA < textB ? -1 : textA > textB ? 1 : 0;
});

for (var i = 0; i < data.length; i++) {
  html2 += "<article class=\"pizzaThumbnail\">\n        <a href=\"".concat(data[i].image, "\">\n            <img src=\"").concat(data[i].image, "\"/>\n            <section>\n                <h4>").concat(data[i].name, "</h4>\n                <ul>\n                    <li>Petit format : ").concat(euro.format(data[i].price_small), "</li>\n                    <li>Grand format : ").concat(euro.format(data[i].price_large), "</li>\n                </ul>\n            </section>\n        </a>\n    </article>\n    ");
}

document.querySelector('.pageContainer').innerHTML = html2;